import pytest

from scielo_stylechecker import create_app


@pytest.fixture
def app():
    return create_app()
