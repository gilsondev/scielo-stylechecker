def test_should_return_status_200(app):
    with app.test_client() as client:
        response = client.get("/status")
        assert response.status_code == 200
