from flask import Blueprint, jsonify


api_blueprint = Blueprint("api", __name__)


@api_blueprint.route("/", methods=["GET"])
def index():
    return jsonify({"api_version": "0.1.0", "status": "success"})
