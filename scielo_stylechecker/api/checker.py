from flask import Blueprint, jsonify


checker = Blueprint("checker", __name__)


@checker.route("/status", methods=["GET"])
def index():
    """
    Show status of API
    ---
    responses:
      200:
        description: Return informations about API
    """
    return jsonify({"api_version": "0.1.0", "status": "success"})
