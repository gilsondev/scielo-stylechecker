import os

from flask import Flask
from flasgger import Swagger


def create_app():
    app = Flask(__name__)
    Swagger(app)

    # Config
    settings = os.getenv("APP_SETTINGS")
    app.config.from_object(settings)

    # Blueprints
    from scielo_stylechecker.api.checker import checker as checker_blueprint

    app.register_blueprint(checker_blueprint)

    # Shell context for cli
    def context():  # noqa
        return {"app": app}

    return app
