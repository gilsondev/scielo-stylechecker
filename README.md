# Scielo Stylechecker

Esse projeto foi criado para validar os periódicos científicos das editoras, no formato de especificação [Scielo PS]().

O usuário poderia fazer o upload do XML, e ele irá apontar quais partes do documentos não estão conforme o padrão.

## Stack

- Python 3.7.1
- Flask 1.0.2
- SQL Alchemy 1.2.14
- Docker
- Swagger
